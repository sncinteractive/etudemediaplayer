package com.tune.media.player.etudemediaplayer.component.media;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.exoplayer2.BuildConfig;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.tune.media.player.etudemediaplayer.R;
import com.tune.media.player.etudemediaplayer.assist.Configuration;
import com.tune.media.player.etudemediaplayer.framework.utils.Utils;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import static android.R.attr.type;

/**
 * Created by youngmincho on 2017. 4. 19..
 */

public class MediaPlayerActivity extends Activity implements View.OnClickListener, ExoPlayer.EventListener,
		    PlaybackControlView.VisibilityListener{
	private static final String TAG = "MediaPlayerActivity";

	public static final String MEDIA_PARAM_FILE_NAME = "MEDIA_PARAM_FILE_NAME";

	private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
	private static final CookieManager DEFAULT_COOKIE_MANAGER;
	static {
		DEFAULT_COOKIE_MANAGER = new CookieManager();
		DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
	}

	private String mFileName;

	private Handler mMainHandler;
	private SimpleExoPlayerView mExoPlayerView;
	private SimpleExoPlayer mPlayer;
	private DefaultTrackSelector mTrackSelector;

	private DataSource.Factory mMediaDataSourceFactory;

	private String mUserAgent;

	private boolean mShouldAutoPlay;
	private boolean mNeedRetrySource;
	private int mResumeWindow;
	private long mResumePosition;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			mUserAgent = Util.getUserAgent(this, getString(R.string.app_name));

			mShouldAutoPlay = true;
			clearResumePosition();
			mMediaDataSourceFactory = buildDataSourceFactory(true);
			mMainHandler = new Handler();
			if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
				CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
			}

			setContentView(R.layout.activity_media_player);

			Intent intent = getIntent();
			if (intent == null) {
				Toast.makeText(this, "미디어 파일 정보가 없습니다.", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

			mFileName = intent.getStringExtra(MEDIA_PARAM_FILE_NAME);
			if(mFileName.isEmpty()) {
				Toast.makeText(this, "미디어 파일 정보가 없습니다.", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

			mFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + Configuration.MEDIA_PATH + mFileName;

			mExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.player_view);
			mExoPlayerView.setControllerVisibilityListener(this);
			mExoPlayerView.requestFocus();

			findViewById(R.id.btnBack).setOnClickListener(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean useExtensionRenderers() {
		return BuildConfig.FLAVOR.equals("withExtensions");
	}

	private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
		return buildDataSourceFactory(useBandwidthMeter ? BANDWIDTH_METER : null);
	}

	public DataSource.Factory buildDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
		return new DefaultDataSourceFactory(this, bandwidthMeter,
				buildHttpDataSourceFactory(bandwidthMeter));
	}

	public HttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
		return new DefaultHttpDataSourceFactory(mUserAgent, bandwidthMeter);
	}

	private void clearResumePosition() {
		try {
			mResumeWindow = C.INDEX_UNSET;
			mResumePosition = C.TIME_UNSET;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void releasePlayer() {
		try {
			if (mPlayer != null) {
				mShouldAutoPlay = mPlayer.getPlayWhenReady();
				updateResumePosition();
				mPlayer.release();
				mPlayer = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateResumePosition() {
		try {
			mResumeWindow = mPlayer.getCurrentWindowIndex();
			mResumePosition = mPlayer.isCurrentWindowSeekable() ? Math.max(0, mPlayer.getCurrentPosition()) : C.TIME_UNSET;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
		try {
			int type = TextUtils.isEmpty(overrideExtension) ? Util.inferContentType(uri)
					: Util.inferContentType("." + overrideExtension);
			switch (type) {
				case C.TYPE_SS:
					return new SsMediaSource(uri, buildDataSourceFactory(false),
							new DefaultSsChunkSource.Factory(mMediaDataSourceFactory), mMainHandler, null);
				case C.TYPE_DASH:
					return new DashMediaSource(uri, buildDataSourceFactory(false),
							new DefaultDashChunkSource.Factory(mMediaDataSourceFactory), mMainHandler, null);
				case C.TYPE_HLS:
					return new HlsMediaSource(uri, mMediaDataSourceFactory, mMainHandler, null);
				case C.TYPE_OTHER:
					return new ExtractorMediaSource(uri, mMediaDataSourceFactory, new DefaultExtractorsFactory(), mMainHandler, null);
				default: {
					throw new IllegalStateException("Unsupported type: " + type);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalStateException("Unsupported type: " + type);
		}
	}

	private void initializePlayer() {
		try {
			Intent intent = getIntent();
			boolean needNewPlayer = mPlayer == null;
			if (needNewPlayer) {
				@SimpleExoPlayer.ExtensionRendererMode int extensionRendererMode = SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF;
				TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
				mTrackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
				mPlayer = ExoPlayerFactory.newSimpleInstance(this, mTrackSelector, new DefaultLoadControl(), null, extensionRendererMode);
				mPlayer.addListener(this);

				mExoPlayerView.setPlayer(mPlayer);
				mExoPlayerView.setUseController(false);

				mPlayer.setPlayWhenReady(mShouldAutoPlay);
			}

			if (needNewPlayer || mNeedRetrySource) {
				MediaSource mediaSource = buildMediaSource(Uri.parse(mFileName), null);
				boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;
				if (haveResumePosition) {
					mPlayer.seekTo(mResumeWindow, mResumePosition);
				}
				mPlayer.prepare(mediaSource, !haveResumePosition, false);
				mNeedRetrySource = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showToast(int messageId) {
		showToast(getString(messageId));
	}

	private void showToast(String message) {
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	}

	private static boolean isBehindLiveWindow(ExoPlaybackException e) {
		if (e.type != ExoPlaybackException.TYPE_SOURCE) {
			return false;
		}
		Throwable cause = e.getSourceException();
		while (cause != null) {
			if (cause instanceof BehindLiveWindowException) {
				return true;
			}
			cause = cause.getCause();
		}
		return false;
	}

	Handler mFinishHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			finish();
		}
	};

	@Override
	public void onNewIntent(Intent intent) {
		try {
			Log.e(TAG, "onNewIntent()");

			releasePlayer();
			mShouldAutoPlay = true;
			clearResumePosition();
			setIntent(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		try {
			Log.e(TAG, "onStart()");

			super.onStart();
			if (Util.SDK_INT > 23) {
				initializePlayer();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		Log.e(TAG, "onResume()");

		super.onResume();

		Utils.hideAllSystemUI(this);

		if ((Util.SDK_INT <= 23 || mPlayer == null)) {
			initializePlayer();
		}
	}

	@Override
	public void onPause() {
		Log.e(TAG, "onPause()");

		super.onPause();
		if (Util.SDK_INT <= 23) {
			releasePlayer();
		}
	}

	@Override
	public void onStop() {
		Log.e(TAG, "onStop()");

		super.onStop();
		if (Util.SDK_INT > 23) {
			releasePlayer();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			initializePlayer();
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		// Show the controls on any key event.
		mExoPlayerView.showController();
		// If the event was not handled then see if the player view can handle it as a media key event.
		return super.dispatchKeyEvent(event) || mExoPlayerView.dispatchMediaKeyEvent(event);
	}

	// OnClickListener methods

	@Override
	public void onClick(View view) {
		try {
			switch(view.getId()) {
				case R.id.btnBack :
					finish();
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTimelineChanged(Timeline timeline, Object manifest) {
	}

	@Override
	public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
		MappingTrackSelector.MappedTrackInfo mappedTrackInfo = mTrackSelector.getCurrentMappedTrackInfo();
		if (mappedTrackInfo != null) {
			if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
					== MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
				showToast(R.string.error_unsupported_video);
			}
			if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO)
					== MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
				showToast(R.string.error_unsupported_audio);
			}
		}
	}

	@Override
	public void onLoadingChanged(boolean isLoading) {
	}

	@Override
	public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
		switch (playbackState) {
			case ExoPlayer.STATE_IDLE :
				Log.e(TAG, "STATE_IDLE - playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
				break;
			case ExoPlayer.STATE_READY:
				Log.e(TAG, "STATE_READY - playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
				break;
			case ExoPlayer.STATE_BUFFERING:
				Log.e(TAG, "STATE_BUFFERING - playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);
				break;
			case ExoPlayer.STATE_ENDED:
				Log.e(TAG, "STATE_ENDED - playWhenReady : " + playWhenReady + ", playbackState : " + playbackState);

				mFinishHandler.sendEmptyMessageDelayed(1, 1500);
				break;
			default :
				break;
		}
	}

	@Override
	public void onPlayerError(ExoPlaybackException e) {
		String errorString = null;
		if (e.type == ExoPlaybackException.TYPE_RENDERER) {
			Exception cause = e.getRendererException();
			if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
				// Special case for decoder initialization failures.
				MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
						(MediaCodecRenderer.DecoderInitializationException) cause;
				if (decoderInitializationException.decoderName == null) {
					if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
						errorString = getString(R.string.error_querying_decoders);
					} else if (decoderInitializationException.secureDecoderRequired) {
						errorString = getString(R.string.error_no_secure_decoder,
								decoderInitializationException.mimeType);
					} else {
						errorString = getString(R.string.error_no_decoder,
								decoderInitializationException.mimeType);
					}
				} else {
					errorString = getString(R.string.error_instantiating_decoder,
							decoderInitializationException.decoderName);
				}
			}
		}
		if (errorString != null) {
			showToast(errorString);
		}
		mNeedRetrySource = true;
		if (isBehindLiveWindow(e)) {
			clearResumePosition();
			initializePlayer();
		} else {
			updateResumePosition();
		}
	}

	@Override
	public void onPositionDiscontinuity() {
		if (mNeedRetrySource) {
			// This will only occur if the user has performed a seek whilst in the error state. Update the
			// resume position so that if the user then retries, playback will resume from the position to
			// which they seeked.
			updateResumePosition();
		}
	}

	@Override
	public void onVisibilityChange(int visibility) {

	}

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		try {
			if(hasFocus) {
				super.onWindowFocusChanged(hasFocus);
				Utils.hideAllSystemUI(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
