package com.tune.media.player.etudemediaplayer.component.intro;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;

import com.tune.media.player.etudemediaplayer.R;

/**
 * Created by youngmincho on 2017. 4. 19..
 */

public class SplashActivity extends Activity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash);

		mHandler.sendEmptyMessageDelayed(1, 1500);
	}

	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			finish();
		}
	};
}
