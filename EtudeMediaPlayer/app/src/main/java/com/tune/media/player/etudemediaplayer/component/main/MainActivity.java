package com.tune.media.player.etudemediaplayer.component.main;

import android.Manifest;
import android.animation.Animator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import com.tune.media.player.etudemediaplayer.R;
import com.tune.media.player.etudemediaplayer.assist.Configuration;
import com.tune.media.player.etudemediaplayer.component.media.MediaPlayerActivity;
import com.tune.media.player.etudemediaplayer.framework.utils.Utils;

import java.io.File;

import static com.tune.media.player.etudemediaplayer.assist.Configuration.REQUEST_CODE_STORAGE_PERMISSION;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
	private static final String TAG = "MainActivity";

	private int mCurrentAnimIndex;

	enum MEDIA_LIST {
		MEDIA_1 (R.id.MEDIA_1, "1.mp4"),
		MEDIA_2 (R.id.MEDIA_2, "2.mp4"),
		MEDIA_3 (R.id.MEDIA_3, "3.mp4"),
		MEDIA_4 (R.id.MEDIA_4, "4.mp4"),
		MEDIA_5 (R.id.MEDIA_5, "5.mp4"),
		MEDIA_6 (R.id.MEDIA_6, "6.mp4"),
		MEDIA_7 (R.id.MEDIA_7, "7.mp4"),
		MEDIA_8 (R.id.MEDIA_8, "8.mp4"),
		MEDIA_9 (R.id.MEDIA_9, "9.mp4");

		int mediaMenuId;
		String mediaResFileName;

		private MEDIA_LIST(int mediaMenuId, String mediaResFileName) {
			this.mediaMenuId = mediaMenuId;
			this.mediaResFileName = mediaResFileName;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			setContentView(R.layout.activity_main);

			mCurrentAnimIndex = 0;

//			startActivity(new Intent(MainActivity.this, SplashActivity.class));

			initView();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initView() {
		try {
			createDefaultMediaFolder();

			for(MEDIA_LIST media : MEDIA_LIST.values()) {
				findViewById(media.mediaMenuId).setOnClickListener(this);
			}

			startMenuAnim(mCurrentAnimIndex);
			startTouchAnim();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private View getAnimView(int _currentIndex) {
		View currentMenuView = null;

		try {
			int index = 0;
			for(MEDIA_LIST media : MEDIA_LIST.values()) {
				if(index == _currentIndex) {
					currentMenuView = findViewById(media.mediaMenuId);
				}

				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return currentMenuView;
	}

	private void startMenuAnim(int _currentIndex) {
		try {
			final View currentMenuView = getAnimView(_currentIndex);

			if(currentMenuView != null) {
				currentMenuView.animate().setDuration(1000).scaleX(0.9f).scaleY(0.9f).setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
					@Override
					public void onAnimationStart(Animator animator) {

					}

					@Override
					public void onAnimationEnd(Animator animator) {
						try {
							currentMenuView.animate().setDuration(1000).scaleX(1f).scaleY(1f).setInterpolator(new LinearInterpolator()).setListener(new Animator.AnimatorListener() {
								@Override
								public void onAnimationStart(Animator animation) {

								}

								@Override
								public void onAnimationEnd(Animator animation) {
									mCurrentAnimIndex++;
									if(mCurrentAnimIndex >= 9) {
										mCurrentAnimIndex = 0;
									}

									startMenuAnim(mCurrentAnimIndex);
								}

								@Override
								public void onAnimationCancel(Animator animation) {

								}

								@Override
								public void onAnimationRepeat(Animator animation) {

								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onAnimationCancel(Animator animator) {

					}

					@Override
					public void onAnimationRepeat(Animator animator) {

					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startTouchAnim() {
		try {
			View touchView = findViewById(R.id.ic_touch);
//			touchView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate));

			Animation an = new RotateAnimation(-10f, 10f, 50f, 10f);

			// Set the animation's parameters
			an.setDuration(1000);               // duration in ms
			an.setRepeatCount(-1);                // -1 = infinite repeated
			an.setRepeatMode(Animation.REVERSE); // reverses each repeat
			an.setFillAfter(true);               // keep rotation after animation

			// Aply animation to image view
			touchView.setAnimation(an);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stopAnim() {

	}

	private void createDefaultMediaFolder() {
		try {
			if(checkStoragePermission()) {
				File fileDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Configuration.DATA_ROOT_PATH);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}

				File fileMediaDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Configuration.MEDIA_PATH);
				if (!fileMediaDir.exists()) {
					fileMediaDir.mkdirs();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startMediaPlayPage(String _fileName) {
		try {
			Bundle bundleAnimation =
					ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.pull_in_right, R.anim.push_out_left).toBundle();

			Intent newIntent = new Intent(MainActivity.this, MediaPlayerActivity.class);
			newIntent.putExtra(MediaPlayerActivity.MEDIA_PARAM_FILE_NAME, _fileName);

			startActivity(newIntent, bundleAnimation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			switch (v.getId()) {
				case R.id.MEDIA_1 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_1.mediaResFileName);
					break;
				case R.id.MEDIA_2 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_2.mediaResFileName);
					break;
				case R.id.MEDIA_3 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_3.mediaResFileName);
					break;
				case R.id.MEDIA_4 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_4.mediaResFileName);
					break;
				case R.id.MEDIA_5 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_5.mediaResFileName);
					break;
				case R.id.MEDIA_6 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_6.mediaResFileName);
					break;
				case R.id.MEDIA_7 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_7.mediaResFileName);
					break;
				case R.id.MEDIA_8 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_8.mediaResFileName);
					break;
				case R.id.MEDIA_9 :
					startMediaPlayPage(MEDIA_LIST.MEDIA_9.mediaResFileName);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		try {
			switch(requestCode) {
				case Configuration.REQUEST_CODE_STORAGE_PERMISSION :
					if(checkPermissionGranted(grantResults)) {
						createDefaultMediaFolder();
					}
					break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public boolean checkStoragePermission() {
		boolean isPermissionGranted = true;

		try {
			if(Build.VERSION.SDK_INT >= 23) {
				if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
						|| checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
					requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_CODE_STORAGE_PERMISSION);
					isPermissionGranted = false;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			isPermissionGranted = false;
		}

		return isPermissionGranted;
	}

	private boolean checkPermissionGranted(int[] grantResults) {
		try {
			if(grantResults.length > 0) {
				boolean isPermissionGranted = true;

				for (int i = 0; i < grantResults.length; i++) {
					if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
						isPermissionGranted = false;
						break;
					}
				}

				return isPermissionGranted;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		try {
			if(hasFocus) {
				super.onWindowFocusChanged(hasFocus);
				Utils.hideAllSystemUI(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		try {
			Utils.hideAllSystemUI(this);
			super.onResume();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
