package com.tune.media.player.etudemediaplayer.assist;

/**
 * Created by youngmincho on 2017. 4. 19..
 */

public class Configuration {
	public static final String DATA_ROOT_PATH = "/ETUDE/";
	public static final String MEDIA_PATH = Configuration.DATA_ROOT_PATH + "MEDIA_DATA/";

	public static final int REQUEST_CODE_STORAGE_PERMISSION = 1000;
}
