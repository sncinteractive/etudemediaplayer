package com.tune.media.player.etudemediaplayer.framework.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;

/**
 * Created by youngmincho on 2017. 4. 21..
 */

public class Utils {
	public static void hideAllSystemUI(Context context) {
		try {
			final int systemUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE;

			final View decorView = ((Activity)context).getWindow().getDecorView();
			decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
				@Override
				public void onSystemUiVisibilityChange(int visibility) {
					try {
						if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
							decorView.setSystemUiVisibility(systemUIFlag);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			decorView.setSystemUiVisibility(systemUIFlag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
